#!/usr/bin/env node

const { exec, execSync } = require('child_process');

const highlightHtml = (html) => {
  let isJson = false;
  let json;
  try {
    json = JSON.parse(html);
    isJson = true;
  } catch (error) {
    isJson = false;
  }
  if(isJson) {
    html = JSON.stringify(json).replace(/"/g, '\\"').replace(/\n/g, ' ');
    const command = `echo "${html}" | jq`;
    (async () => { await execSync(command, { stdio: 'inherit', shell: true }); })();
  } else {
    html = html.replace(/"/g, '\\"').replace(/\n/g, ' ');
    return new Promise((resolve, reject) => {
      const command = `echo "${html}" | pup | pygmentize -l html -f terminal256 -O style=monokai,bg=dark`;
      exec(command, (error, stdout, stderr) => {
        if (error) {
          console.error('Error highlighting HTML:', error);
          reject(error);
        } else {
          console.log(stdout);
          resolve(stdout);
        }
      });
    });
  }
};

const run = async (requests) => {
  const htmlDocs = await main(requests);
  for (let html of htmlDocs) {
    highlightHtml(html);
  }
};

const main = async (requests) => {
  if(Array.isArray(requests)) {
    try {
      let htmlDocs = [];
      for (let request of requests) {
        const response = await request;
        htmlDocs.push(await response.text());
      }
      return htmlDocs;
    } catch (error) {
      console.error(error);
    }
  }
};

module.exports = run;

if(require.main === module) {
  if(process.argv.includes('-h') || process.argv.includes('--html')) {
    let inputChunks = [];
    process.stdin.on('readable', () => {
      let chunk;
      while ((chunk = process.stdin.read()) !== null) {
        inputChunks.push(chunk);
      }
    });
    process.stdin.on('end', () => {
      const html = inputChunks.join('');
      highlightHtml(html);
    });
  } else {
    let urls = process.argv.slice(2).map(url => url.startsWith('http') ? url : `https://${url}`);
    let requests = urls.map(url => fetch(url));
    run(requests);
  }
}
