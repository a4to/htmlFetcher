
# htmlFetcher

`htmlFetcher` is a Node.js command-line utility designed for developers. It simplifies logging HTML content from an array of fetch requests copied directly from the browser and enables fetching and highlighting HTML content from URLs directly from the command line.


## Core Functionality

- **Ease of Use with Browser Fetch Requests**: Tailored for arrays of fetch requests copied from the browser, streamlining the process of logging HTML content.
- **Direct Fetch from URLs**: Easily fetch and highlight HTML content from URLs through the command line.
- **Comprehensive Syntax Highlighting**: Utilizes `pygmentize` and `jq` for enhanced readability of HTML and JSON content.

## Version

- **0.1.3**

## License

- Licensed under the **MIT License**.

## Prerequisites

Before you begin, make sure you have the following installed:
- Node.js ([Download Node.js](https://nodejs.org))
- `pup` for HTML parsing ([GitHub - ericchiang/pup](https://github.com/ericchiang/pup))
- `pygmentize` for syntax highlighting (`pip install Pygments`)
- `jq` for JSON syntax highlighting ([Download jq](https://stedolan.github.io/jq/))

## Installation

```bash
git clone https://gitlab.com/a4to/htmlFetcher.git
cd htmlFetcher
npm install
```

## Usage

### Fetch and Highlight HTML Content from URLs

To fetch and highlight HTML content directly from a URL:

```bash
htmlFetcher example.com
```

This command fetches the HTML content from `example.com`, applies syntax highlighting, and prints it to the console.

### Processing Fetch Requests Array

For logging HTML content from browser-copied fetch requests:

```javascript
const htmlFetcher = require('./htmlFetcher');

// Example array of fetch requests
const requests = [
  fetch('https://example.com'),
  fetch('https://anotherexample.com')
];

htmlFetcher(requests);
```

### Command-line Options

- `--html`: Process HTML or JSON content piped into `htmlFetcher`, useful for either GET or POST requests (where the response may be JSON or HTML).

Example for HTML:

```bash
echo "<html>...</html>" | node htmlFetcher.js --html
```

Example for JSON:

```bash
echo '{"key": "value"}' | node htmlFetcher.js --html
```

## Author

- Connor Etherington
- Email: connor@concise.cc

## Contributing

Your contributions to `htmlFetcher` are highly appreciated. Whether it's through feature requests, bug reports, or pull requests, your feedback and contributions help improve this project.
